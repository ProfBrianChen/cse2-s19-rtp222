//////////////
///Ryan Peterson CSE2 Feb 10th, 2019
//////////////
// This program will measure the minutes for each trip, the counts for each trip, the distance of each trip in miles, and distance of the combined trips

public class Cyclometer {
  public static void main(String[] args){
    
    int secsTrip1=480; //stores how long Trip #1 was
    int secsTrip2=3220; //stores how long Trip #2 was
    int countsTrip1=1561; //stores how many counts in trip 1
    int countsTrip2=9037; //stores how many counts in trp 2
    
    double wheelDiameter=27.0; //stores the diameter of the wheel 
    double PI=3.14159; // stores the value of pi
    double feetPerMile=5280; //number of feet per mile
    double inchesPerFoot=12; // number of inches per foot
    double secondsPerMinute=60; //number of seconds per minute
    double distanceTrip1, distanceTrip2, totalDistance; // stores different variables
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+"minutes and had "+ countsTrip1+"counts");//print out the information for trip 1 
    System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+"minutes and had "+ countsTrip2+"counts");//print out the information for trip 2
    distanceTrip1=countsTrip1*wheelDiameter*PI/inchesPerFoot/feetPerMile;//doing the conversions to find the distance traveled in miles for trip one
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;//doing the conversions to find the distance traveled in miles for trip two
    totalDistance=distanceTrip2+distanceTrip1;// finding the total distance from both trips
    System.out.println("Trip 1 was "+distanceTrip1+"miles"); //printing miles for trip 1
    System.out.println("Trip 2 was "+distanceTrip2+"miles"); //printing miles for trip 2
    System.out.println("The total distance was "+totalDistance+"miles"); //prints the total miles
  }
}