////////
// Ryan peterson CSE hw06
////////
// This program will utilize loops to print out a complex network window pattern with specifications provided by the user




//I was unable to figure out how to get vertical side lengths of the squares to appear correctly. The very left side works fine as it is the first one, 
//but then I thought to implement the code for the vertical sides, where they would be placed in the horizontal line code. But due to the println function,
//necessary for the vertial line code, the next horizontal segment would print at the bottom of the vertical lines, not directly across from the previous 
//horizontal line. I am not sure if I went about creating this code in the correct way, or if I have backed myself into a corner with no means of fixing
// it other than to start all over again with a new approach, such as constructing code to go line by line downwards rather than attempting to do both 
//directions at once, which I am not sure would work either.


import java.util.Scanner; //imports the scanner class to be used in this code

public class Network{
   public static void main (String args[]){
      Scanner myScanner= new Scanner (System.in); //initializing the scanner
     
     String junkWord;
     
     int height;
     int width;
     int size;         //all of these are initializing variables
     int length;
     int realSize;
     
     int i;
     int j;
     int k;
     int x;
     int y;         //all of these are initializing variables
     int z;
     int a;
     int b;
     int c;
     
     
     System.out.println("Enter a positive integer 'height' ");  //prompting the user for the height
    while(!myScanner.hasNextInt()){  //while the input is not an integer, there will be an error
      junkWord= myScanner.next();//accepts a string
      System.out.println("Uh oh. An error occured. Please enter a positive integer");// prints out an error statement
    }
     height=myScanner.nextInt();// accepts an integer
     while(height<0){
       System.out.println("Uh oh. An error occured. Please enter a positive integer");
       height=myScanner.nextInt();
     }
     
     
     
      System.out.println("Enter a positive integer 'width' ");  //prompting the user for the width
    while(!myScanner.hasNextInt()){  //while the input is not an integer, there will be an error
      junkWord= myScanner.next();//accepts a string
      System.out.println("Uh oh. An error occured. Please enter a positive integer");// prints out an error statement
    }
     width=myScanner.nextInt();// accepts an integer
     while(width<0){
       System.out.println("Uh oh. An error occured. Please enter a positive integer");
       width=myScanner.nextInt();
     }
     
     
     
      System.out.println("Enter a positive integer 'size' ");  //prompting the user for the size
    while(!myScanner.hasNextInt()){  //while the input is not an integer, there will be an error
      junkWord= myScanner.next();//accepts a string
      System.out.println("Uh oh. An error occured. Please enter a positive integer");// prints out an error statement
    }
     size=myScanner.nextInt();// accepts an integer
     while(size<0){
       System.out.println("Uh oh. An error occured. Please enter a positive integer");
       size=myScanner.nextInt();
     }
     
     
     
     System.out.println("Enter a positive integer 'length' ");  //prompting the user for the length
    while(!myScanner.hasNextInt()){  //while the input is not an integer, there will be an error
      junkWord= myScanner.next();//accepts a string
      System.out.println("Uh oh. An error occured. Please enter a positive integer");// prints out an error statement
    }
     length=myScanner.nextInt();// accepts an integer
     while(length<0){
       System.out.println("Uh oh. An error occured. Please enter a positive integer");
       length=myScanner.nextInt();
     }
     
     realSize = size-2;// getting the length in betweeen the corners of the squares
     
   
     
     
     
     
     for(x=2;x<height;){//starts the loop for the vertical lines
         
       
    
         for(i=1;i<=width;){// prints out the very top horizontal line pattern
         
         System.out.print("#");//starts a corner
         i++;//accounts for lenght of the corner 
         for(j=1;j<=realSize;j++){//starts the loop for the side lenght
           System.out.print("-"); //prints out one space
           i++;//accounts for length of space
           if (i==width){ break; }//stops printing if the width is reached
         }
         System.out.print("#");//adds a corner
         i++;//accounts for length of corner
         for(k=1;k<=length;k++){//starts loop for the spaces in between squares
           System.out.print(" ");//prints spaces
           i++;//accounts for length of space
           if(i==width){break;}//stops printing if the width is reached
         }
           
     }
    
       
         for(y=1;y<=realSize;y++){//starts loop for vertical line pattern
           System.out.println("|"); //prints one line
           x++;//accounts for height of line
           if (x==height){ break; }//if height is reached, printing stops
         }
         
       
       
    
       
       for(i=1;i<=width;){//starts the loop for the horizontal lines pattern
         
         System.out.print("#");//starts off with a corner
         i++;//accounts for the space of the corner and adds one to the width counter 'i'
         for(j=1;j<=realSize;j++){//starts the loop for the side lenght
           System.out.print("-"); //prints out one space
           i++;//accounts for length of space
           if (i==width){ break; }//stops printing if the width is reached
         }
         System.out.print("#");//adds a corner
         i++;//accounts for length of space
         for(k=1;k<=length;k++){//starts loop for the spaces in between squares
           System.out.print(" ");//prints out one space
           i++;//accounts for length of space
           if(i==width){break;}//stops printing if the width is reached
         }
           
     }
       
       
       
       
       
       
         for(z=1;z<=length;z++){//starts loop for spaces in between squares
           System.out.println(" ");//prints space
           x++;//accounts for height of space
           if(x==height){break;}//if height is reached, printing stops
         }
           
     }
     
     
     
     
     
     
     
     
   }//end of main method
}//end of class


