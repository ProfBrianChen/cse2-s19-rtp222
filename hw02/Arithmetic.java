/////////////
//Ryan Peterson CSE2 Feb 10th, 2019
/////////////
//this program will manipulate data that is stored in variables, run calculations, and print the results of the calculations
public class Arithmetic{
  
  public static void main (String args []){
    // Number of pairs of pants
    int numPants = 3;
    //cost per pair of pants
    double pantsPrice = 34.98;
    
    //number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //number of belts
    int numBelts = 1;
    //cost per belt
    double beltPrice = 33.99;
    
    //the tax rate in PA
    double paSalesTax = 0.06;
    
    //Total cost of pants
    double pantsCost1 = numPants * pantsPrice;
    
    //total cost of shirts
    double shirtsCost1=numShirts*shirtPrice;
    
    //total cost of belts
    double beltsCost1=numBelts*beltPrice;
    
    //Sales tax on pants 
    double taxPants=paSalesTax*pantsCost1;
    
    //Sales tax on Shirts
    double taxShirts=paSalesTax*shirtsCost1;
    
    //sales tax on belts 
    double taxBelts=paSalesTax*beltsCost1;
    
    //total cost of purchase before tax
    double costBefore=pantsCost1+shirtsCost1+beltsCost1;
    
    //total sales tax
    double totalTax=taxPants+taxBelts+taxShirts;
    
    //total Cost of everything
    double totalCost=costBefore+totalTax;
    
    System.out.println("cost of pants before tax " + String.format("%.2f",pantsCost1));
    System.out.println("Tax on pants " + String.format("%.2f",taxPants));
    System.out.println("cost of sweatshirts before tax " + String.format("%.2f",shirtsCost1));
    System.out.println("Tax on sweatshirts " + String.format("%.2f",taxShirts));
    System.out.println("Cost of belts before tax " + String.format("%.2f",beltsCost1));
    System.out.println("Tax on belts " + String.format("%.2f",taxBelts));
    System.out.println("Total cost before tax " + String.format("%.2f",costBefore));
    System.out.println("Total Sales tax charged " + String.format("%.2f",totalTax));
    System.out.println("Total cost of everything including tax " + String.format("%.2f",totalCost));
    
    
  }
}