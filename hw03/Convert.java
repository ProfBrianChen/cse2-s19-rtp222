//////
//Ryan Peterson CSE2 Feb 12, 2019
//////
//This program will convert the inputed number of meters into inches
import java.util.Scanner;// looked up online how to add a data input method to java
  
public class Convert {
  public static void main (String args[]){
    Scanner myScanner = new Scanner (System.in);//prompting for input, found in lab 3
    System.out.print("Enter the number of meters in the form xx.xx");//asking the user for input in a specific form xx.xx
    double numberMeters= myScanner.nextDouble();// assigns number of meters entered by user
    //conversion factor for meters to inches is 39.37 inches per meter
    double numberInches= numberMeters*39.37; //converts meters to inches
    System.out.println("This many meters,"+numberMeters+", is this many inches,"+ numberInches);//
    
    
    
    
  }
}
