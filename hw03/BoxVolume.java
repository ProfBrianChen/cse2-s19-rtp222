///////////
//Ryan Peterson CSE2 Feb 11th, 2019
///
//This program is going to take the dimensions of a box that were inputed by a user, and then will calculate the volume

import java.util.Scanner;//from lab three, allows the input of a variable

public class BoxVolume {
  public static void main (String args[]){
    Scanner myScanner = new Scanner (System.in);//prompting for input, found in lab 3
    System.out.print("Enter the value for the width ");//prompting for the width of the box 
    double numWidth=myScanner.nextDouble();//allows the user to input width
    System.out.print("Enter the value for the length ");// prompts for the length
    double numLength=myScanner.nextDouble();//allows user to enter length
    System.out.print("Enter the value for the height ");//prompting for the height
    double numHeight=myScanner.nextDouble();//allows user to enter height
    double numVolume=numHeight*numLength*numWidth;// calculating the volume
    System.out.println("The volume inside the box is:"+numVolume);//printing out the volume of the box
    
  }
}
