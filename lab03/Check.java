////////
//Ryan Peterson CSE2 Feb 13th, 2019
////////
//This program will split the check of a dinner evenly, including tip, based on numbers inputed by the user

import java.util.Scanner;

public class Check{
  //changed the class to check becuase it is the name of the file and cookies did not work
  public static void main(String[] args){
     
  Scanner myScanner= new Scanner( System.in );//allows inputs to be entered
  System.out.print("Enter the original cost of the check in the form xx.xx");//asks the user for the cost of the meal
  double checkCost = myScanner.nextDouble();//allows the user to enter the cost
  System.out.print("Enter teh percentage tip you wish to pay as a whole number (in the form xx) :");//asks user for the tip percentage
  double tipPercent = myScanner.nextDouble();//allows user to enter tip value
  tipPercent /= 100; //we want to convert the percentage into a decimal
  System.out.print("Enter the number of people who went out to dinner:" );//prompts for number of diners
  int numPeople = myScanner.nextInt();//allows user to enter an integer for number of people
  double totalCost;
  double costPerPerson;
  int dollars;  //whole dollar amount of cost
  int dimes; //for storing digits to the right of the decimal
  int pennies;// for storing digits to the right of the decimal point
  totalCost= checkCost*(1+ tipPercent);
  costPerPerson= totalCost/numPeople;
  dollars=(int)costPerPerson;
  dimes=(int)(costPerPerson*10)%10;
  pennies=(int)(costPerPerson*100)%10;
  System.out.println("Each person in the group owes $" +dollars +"." + dimes + pennies);
  
  }
    
    
  }
 
  

