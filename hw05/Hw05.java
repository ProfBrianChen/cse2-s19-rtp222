////////
// Ryan Peterson CSE2 Hw 5
////////
//This code will prompt the user to input information about a specific class they are taking, and utilize loops to make sure that the information they entered was in the correct format

import java.util.Scanner; //imports the scanner class to be used in this code
  
public class Hw05 {
 
  public static void main (String args []){
    Scanner myScanner= new Scanner (System.in); //initializing the scanner
    
    
    String error;//initializing variable for if there is an error
    int courseNum;//initializing variable for the course number
    String deptName;//initializing variable for the department name
    int numPerWeek;//initializing variable for the number of times the class meets per week
    String startTime;//initializing variable for the class start time
    String profName;//initializing variable for the professors name
    int numStudents;//initializing variable for the number of students in the class
    
    System.out.println("Enter the course number for a course you are currently enrolled in ");  //prompting the user for the course number
    while(!myScanner.hasNextInt()){  //while the input is not an integer, there will be an error
      error= myScanner.next();//accepts a string
      System.out.println("Uh oh. An error occured. Please enter an integer");// prints out an error statement
    }
     courseNum=myScanner.nextInt();// accepts an integer
    
   
    
    System.out.println("Enter the name of the department");//prompting the user for the department name
    while(myScanner.hasNextInt()){ //while the input is an integer, there will be an error until a name is entered
      deptName= myScanner.next();//accepts a string
      System.out.println("Uh oh. An error occured. Please enter a name");// prints out an error statement
    }
      error= myScanner.next();//accepts a string
 
    
    
    System.out.println("Enter the number of times the class meets per week ");  //prompting the number of times the class meets per week
    while(!myScanner.hasNextInt()){ //while the input is not an integer, there will be an error
      error= myScanner.next();//accepts a string
      System.out.println("Uh oh. An error occured. Please enter an integer");// prints out an error statement
    }
     numPerWeek= myScanner.nextInt();// accepts an integer
    
    
  
    System.out.println("Enter the start time for the class in the format xx:yy. i.e 10:15 a.m");//asking for the class startime
    while(myScanner.hasNextInt()){ //while the input is an integer, there will be an error until a name is entered
      startTime= myScanner.next();//accepts a string
      System.out.println("Uh oh. An error occured. Please make sure your formatting is correct");// prints out an error statement
    }
      error= myScanner.next();//accepts a string
   
    
    
    
    System.out.println("Enter the number of students in the class");  //prompting the user for the number of students in the class
    while(!myScanner.hasNextInt()){ //while the input is not an integer, there will be an error
      error= myScanner.next();//accepts a string
      System.out.println("Uh oh. An error occured. Please enter an integer");// prints out an error statement
    }
     numStudents=myScanner.nextInt();// accepts an integer
    
    
    
    
    System.out.println("Enter the name of the professor");// asking for  the name of the professor
    while(myScanner.hasNextInt()){ //while the input is an integer, there will be an error until a name is entered
      profName= myScanner.next();//accepts a string
      System.out.println("Uh oh. An error occured. Please enter a name");// prints out an error statement
    }
      error= myScanner.next();//accepts a string
    
    
    
    
    
  
       }//end of the main method
    }// end of the class
    
    
      
     
  
  

  
