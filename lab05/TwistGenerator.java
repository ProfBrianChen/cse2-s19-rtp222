////////
// Ryan peterson CSE Lab 05
////////
// This program will utilize loops to print out a simple twist on the screen


import java.util.Scanner; //imports the scanner class to be used in this code


public class TwistGenerator{
   public static void main (String args[]){
      Scanner myScanner= new Scanner (System.in); //initializing the scanner
     
     String junkWord;
     int length;
     int x=3;
     
     
    System.out.println("Enter a positive integer named 'length' ");  //prompting the user for the course number
    while(!myScanner.hasNextInt()){  //while the input is not an integer, there will be an error
      junkWord= myScanner.next();//accepts a string
      System.out.println("Uh oh. An error occured. Please enter a positive integer");// prints out an error statement
    }
     length=myScanner.nextInt();// accepts an integer
     if(length<0){
       System.out.println("Uh oh. An error occured. Please enter a positive integer");
       length=myScanner.nextInt();
     }
     
     
     
     System.out.print("\\ "); 
     for(x=3;x<length;x=x+3){
       System.out.print("/\\ ");  
     }
     System.out.println("");
     
     
     
     System.out.print(" ");
     for(x=3;x<length;x=x+3){
       System.out.print("X  ");  
     }
     System.out.println("");
     
     
     
     System.out.print("/ ");
     for(x=3;x<length;x=x+3){
       System.out.print("\\/ ");  
     }
     System.out.println("");
   }
}
 